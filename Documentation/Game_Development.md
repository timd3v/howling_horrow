# Howling Horrow

**Table of contents**

- [Howling Horrow](#howling-horrow)
    - [The idea](#the-idea)
    - [Project details](#project-details)
      - [Name](#name)
      - [Game type](#game-type)
      - [Art style](#art-style)
      - [Scope](#scope)
      - [Audience](#audience)
      - [Platform](#platform)
      - [Budget](#budget)
    - [Characters](#characters)
      - [Survivor example 1](#survivor-example-1)
      - [Killer example 1](#killer-example-1)
    - [Game Mechanics](#game-mechanics)
    - [Levels](#levels)
    - [Itemization](#itemization)
    - [UI UX](#ui-ux)
    - [NPCs](#npcs)

### The idea

An online multiplayer that enables players the choice of either playing as a survivor or a killer. The stage is set on killer turf and survivors stand a higher chance of staying alive as a group.

The survivor  
Explore, hunt and gather tools during day time, but hide and set up forts at night.

The killer  
Mentally and physcially deformed predators at night slaughtering survivors once the sun has set.  

Survivors have the advantage of elimnating the killer at his weakest during the day, but stand no chance at night.  
Killers are predators during the nightime with their enhanced capabilities.

### Project details

#### Name  
- Howling Horrow
#### Game type
- 3D isometric
- Horror survival
- Online multiplayer
  
#### Art style
- The art style is 3D isometric that will involve alot of contrasts, simple shapes and modular. Some [examples](https://www.google.com/search?q=3d+isometric+game+horror&sxsrf=ACYBGNRqMLUjDh8cmT_gnEdlbhWcDhDodw:1576430328741&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj504fOlLjmAhUOXMAKHYFGCFsQ_AUoAXoECA4QAw&biw=1920&bih=930#imgdii=8A1hplVgJbXgfM:&imgrc=8A1hplVgJbXgfM:).
  
#### Scope
- MVP - 5 Months
- Stretch - 8 Months
  
#### Audience
- Simplistic
- Casual players
- Mature 13+
- Requires some sort of warning or rule put in place to warn young audience parents.
  
#### Platform
- PC
- Posssibly console, depending on whether it is popular or project time allows for it.
  
#### Budget
- Any budget will go toward marketing the game.

### Characters

#### Survivor example 1

#### Killer example 1

### Game Mechanics

### Levels

### Itemization

### UI UX

### NPCs
